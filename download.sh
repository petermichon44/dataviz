#!/bin/sh

git clone -b development https://gitlab.com/gmva_sspc/meteo_prev.git
git clone -b deploie https://gitlab.com/gmva_sspc/web_app.git
git clone -b docker https://gitlab.com/gmva_sspc/datasrc.git
