# Documentation du dashboard

Cette documentation concerne le dashboard des bâtiments qui sera présent sur l'application web. Le dashboard contiens un ensemble de graphiques qui représente différentes données datés tel que la consommation énergétique des bâtiments, leur température ou les conditions météorologiques.

## Graphiques

### Température moyenne à l'intérieur et à l'extérieur du bâtiment

#### Description

Ce graphique représente la température moyenne de toutes les pièces du bâtiment et la température extérieure moyenne de la ville où se trouve ce bâtiment. Dans cet exemple, c'est le bâtiment 1, situé à Arradon.

![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/dashboard/dashboard1.png "graphique")

#### Requêtes

> La température moyenne à l'intérieur du bâtiment 1

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "indoor")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> La température extérieure

```sql
from(bucket: "OWM")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "ARRADON")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

#### Alertes

À venir...

### Température moyenne et seuils de déclanchement du chauffage/refroidissement

#### Description

Ce graphique représente la température moyenne à l'intérieur du bâtiment 1 comparé aux seuils de déclanchement du chauffage et du refroidissement.

![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/dashboard/dashboard2.png "graphique")

#### Requêtes

> La température et les seuils de chauffage/refroidissement du bâtiment.

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "indoor")
  |> filter(fn: (r) => r["_field"] == "temperature" or r["_field"] == "heating" or r["_field"] == "cooling")
  |> group(columns: ["_field"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

#### Alertes

À venir...

### Consommation énergétique et température moyenne à l'intérieur et à l'extérieur du bâtiment

#### Description

Ce graphique représente la consommation énergétique du bâtiment 1 comparé à la température moyenne à l'intérieur et à l'extérieur du bâtiment.

![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/dashboard/dashboard3.png "graphique")

#### Requêtes

> La température moyenne du bâtiment 1

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "indoor")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> La température extérieure

```sql
from(bucket: "OWM")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "ARRADON")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> La consommation énergétique du bâtiment 1

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "electricite")
  |> filter(fn: (r) => r["_field"] == "Consumption")
  |> filter(fn: (r) => r["hvac"] == "hvac_1")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

#### Alertes

À venir...

### Débit d'air filtré par RTU (Roof Top Unit)

#### Description

Ce graphique représente le débit d'air filtré par chacun des RTUs (Roof Top Units) du bâtiment 1.

![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/dashboard/dashboard4.png "graphique")

#### Requêtes

> Le débit d'air filtré par RTU

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "RTU")
  |> filter(fn: (r) => r["_field"] == "supply_air_flow")
  |> filter(fn: (r) => r["RTU"] == "RTU_001" or r["RTU"] == "RTU_002")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

#### Alertes

À venir...

### Consommation énergétique moyenne

#### Description

Ce graphique représente la consommation énergétique moyenne du bâtiment 1.

![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/dashboard/dashboard5.png "graphique")

#### Requêtes

> La moyenne des consommations énergétiques du bâtiment 1

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "electricite")
  |> filter(fn: (r) => r["_field"] == "Consumption")
  |> filter(fn: (r) => r["hvac"] == "hvac_1")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

#### Alertes

À venir...

### Température par pièce

#### Description

Ce graphique représente la température de chacune des pièces du bâtiment 1.

![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/dashboard/dashboard6.png "graphique")

L'intention derrière ce graphique est que l'utilisateur puisse sélectionner les courbes qui l'intéresse pour laisser apparaitre certaines pièces du bâtiment. Pour sélectionner afficher/cacher une courbe, il suffit de la sélectionner dans la liste à droite du graphique.

#### Requêtes

> La température de chaque pièce du bâtiment 1

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "indoor")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

#### Alertes

## Comparaison des deux bâtiments 

### Concentration moyenne de CO2 des bâtiments 

#### Description

Ce graphique représente l'émission moyenne de CO2 dans chaque bâtiments (en l'occurence le bâtiment 1 et le bâtiment 2)

![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/dashboard/dashboard7.png "graphique")

#### Requêtes

> L'émission moyenne de CO2 du bâtiment 1 

```sql
from(bucket: "batiment1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "co2")
  |> filter(fn: (r) => r["_field"] == "co2")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> L'émission moyenne de CO2 du bâtiment 2

```sql
from(bucket: "batiment2")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "co2")
  |> filter(fn: (r) => r["_field"] == "co2")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

### Température moyenne des bâtiments 

#### Description

Ce graphique représente la température moyenne de chaque bâtiments (en l'occurence le bâtiment 1 et le bâtiment 2). Il permet de comparer les températures des deux bâtiments. 

![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/dashboard/dashboard8.png "graphique")

#### Requêtes

> Température moyenne du bâtiment 1 

```sql
from(bucket: "batiment1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "exterior")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> Température moyenne du bâtiment 2

```sql
from(bucket: "batiment2")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "co2")
  |> filter(fn: (r) => r["_field"] == "co2")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```




