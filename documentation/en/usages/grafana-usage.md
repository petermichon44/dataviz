Grafana User Guide
==================

In this guide, we will show you how to use the **Grafana** software to create graphs, modify them, etc.


### Creating a Graph

> 1 - First in **Grafana**, go to **Dashboard**, **new dashboard**
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation5.png "buckets")

> 2 - First, create a new panel by clicking on **add a new panel** 
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation6.png "buckets")

> 3 - Second, select InfluxDB in **data source** and paste the queries seen in InfluxDB. Filter the time to choose the hourly page of temperature data, and click on **Apply**. You should have a default graph that appears.
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation7.png "buckets")

> 4 - To choose another type of graph, click on **Times Series** and choose from the drop-down list the type of graph you want.
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation8.png "buckets")

> Here are two types of graphs:
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation9.png "buckets")
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation10.png "buckets")
