Guide d'installation d'InfluxDB sous Docker
===========================================

Dans ce guide, nous allons vous guider sur l'installation du logiciel **InfluxDB**, que nous utiliserons dans le cadre de notre projet pour le **Golfe du Morbihan Vannes Agglomération** (GMVA).

**InfluxDB** nous permettra de stocker les données qui seront reçus des différents capteurs, afin de pouvoir les exploiter par la suite avec des tableaux, graphiques et indicateurs pertinents afin d'améliorer le suivi des installations communautaires par **GMVA**.

Sous Windows
---

### I. Vérification de l'installation Docker 
> 1. Pour vérifier que *Docker* est bien installé éxécuter la commande : 
> ```shell
> docker -version
> ```
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation1.png "Page principale")

### II. Installation d'InfluxDB
> 1. Exécutez la commande suivante dans un terminal : 
> ```shell
> docker pull influxdb
> ```
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation2.png "Execution de la commande")
![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation3.png "Résultat obtenu")

### III. Lancement d'InfluxDB
> 1. Exécutez la commande :  
> ```shell
> docker run --name influxdb -p 8086:8086 quay.io/influxdb/influxdb:2.0.0-rc
> ```
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation4.png "Si aucun containers InfluxDB n'existe alors un nouveau sera créer")

### IV. Configuration d'InfluxDB
> 1. Allez sur http://localhost:8086/
> 2. Cliquez sur **"get started"**
> 3. Remplissez les champs nom d'utilisateur, mot de passe, confirmer le mot de passe, donner un nom d'organisation et un nom de bucket
>
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation5.png "Page de configuration")
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation6.png "Page de configuration remplis")

### V. Suite
> [Continuez avec l'installation de Grafana](https://gitlab.com/gmva_sspc/dataviz/-/blob/main/Documentation%20/grafana.md)
