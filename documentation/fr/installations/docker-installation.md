Guide d'installation de Docker
==============================

Dans ce guide, nous allons vous guider sur l'installation de **Docker**, que nous utiliserons dans le cadre de notre projet pour le **Golfe du Morbihan Vannes Agglomération** (GMVA).

**Docker** nous permettra d'héberger nos deux outils **InfluxDB** et **Grafana** afin que nous puissions travailler en collaboration durant la totalité du projet.

Sous Windows
---

### I. Téléchargement de Docker 
> 1. Se rendre sur le site [Docker](https://www.docker.com/products/docker-desktop/)
> 2. Cliquer sur le téléchargement

### II. Installation de Docker 
> 1. Lancer l'installateur
> 2. Après le chargement il vous sera demandé de **"log out"**
> 3. Ensuite, cliquer sur Docker Desktop. Accepter les conditions d'utilisations.
> 4. Cliquer sur **"Start"**

### III. Suite
> [Continuer avec l'installation de Grafana](https://gitlab.com/gmva_sspc/dataviz/-/blob/main/Documentation%20/influxdb.md)
