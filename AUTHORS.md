Les auteurs de ce projet pour la visualisation des données
==========================================================


## Développeurs

* __[Peter Michon](https://gitlab.com/petermichon44) (Scrum Master)__

    Temps passé sur le projet : ```??? heures```

* __[Tobias Dumoulin](https://gitlab.com/tobias.d)__

    Temps passé sur le projet : ```??? heures```

* __[Chadène Kerbous](https://gitlab.com/Chadene)__

    Temps passé sur le projet : ```??? heures```

* __[Romain Quéré](https://gitlab.com/romainqre)__

    Temps passé sur le projet : ```??? heures```


## Tuteur

* __[Minh-Tan Pham](https://gitlab.com/mtpham)__


## Responsable du projet

* __[Nicolas Le Sommer](https://gitlab.com/mtpham)__


## Contributeurs du projet

* __[Application Web](https://gitlab.com/gmva_sspc/web_app)__
* __[Prévision Météorologiques](https://gitlab.com/gmva_sspc/meteo_prev)__
* __[Visualisation des données](https://gitlab.com/gmva_sspc/dataviz)__
